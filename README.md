# INFO

### Accesso workstation
|KEY|VALUE|
|---|-----|
|user:|corso14|
|pass:|@HipHop24|

### Accesso al bastion
|KEY|VALUE|
|---|-----|
|host:|172.24.225.175|
|user:|Cognome|
|pass:|corso2024|

### Accesso cluster
|KEY|VALUE|
|---|-----|
|OCP Cluster API:|https://api.lab1.aci.it:6443|
|OCP Cluster Console:|https://console-openshift-console.apps.lab1.aci.it|
|user:|kubeadmin|
|pass:|hoFi6-Y6Jed-Nq88a-y5kGY|

### Questionario
 [OpenShift Container Platform Features](https://docs.google.com/forms/d/1jwd0062bhqNkbGZGXuhasfinJ2jneS7b2jiGjOb-EnA/edit?pli=1)

### Immagini Repository Pubblico
|KEY|VALUE|
|---|-----|
|wordpress|quay.io/redhattraining/wordpress:5.7-php7.4-apache|
|mysql|quay.io/redhattraining/mysql-80:1-251|
|httpd|docker.io/kerberos5/httpd:2.4|
|postgresql|registry.redhat.io/rhel8/postgresql-12:latest|
|pgadmin|docker.io/dpage/pgadmin4:latest|
|nginx|quay.io/redhattraining/hello-world-nginx:v1.0|
|gitlab|quay.io/redhattraining/gitlab-ce:8.4.3-ce.0|
|ubi8|registry.access.redhat.com/ubi8/httpd-24:latest
|ubi8-httpd24|registry.access.redhat.com/ubi8/httpd-24:latest|
|nodejs-18|registry.access.redhat.com/ubi9/nodejs-18:latest|

### Immagini Repository Privato
|KEY|VALUE|
|---|-----|
|mysql|registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/rhel8/mysql-80|
|wordpress|registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/redhattraining/wordpress|
|httpd|registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/kerberos5/httpd|
|postgresql|registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/rhel8/postgresql-12|
|pgadmin|registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/dpage/pgadmin4|
|nginx|registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/redhattraining/hello-world-nginx|
