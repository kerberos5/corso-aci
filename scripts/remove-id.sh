echo '### Verifico utenti presenti'
for delnames in $(oc get users | awk '{print $1}' | grep -E -v 'admin|kubeadmin|developer|NAME'); do
  echo "Elimino utente: $delnames"
  # Trova le identità associate all'utente
  identities=$(oc get user $delnames -o jsonpath='{.identities[*]}')
  
  # Elimina ciascuna identità associata
  for identity in $identities; do
    echo "Elimino identità: $identity"
    oc delete identity $identity
  done
  
  # Elimina l'utente
  oc delete user $delnames
done

echo '### Utenti e identità rimossi con successo!'
oc get users
oc get identities
