#!/bin/bash
echo '### Verifico progetti presenti'
for delnamespace in $(oc get project | grep -E -v 'openshift|kube|default|group-sync-operator|hostpath-provisioner|NAME' | awk '{print $1}'); do
  oc delete project $delnamespace
done
echo '### Namespace rimossi con successo!'
oc get project | grep -E -v 'openshift|kube' | awk '{print $1}'

echo '### Verifico i PV presenti da rimuovere'
for delpv in $(oc get pv | grep -E -v '^pvc-|NAME' | awk '{print $1}'); do
  oc delete pv $delpv
done
echo '### PV rimossi con successo!'
oc get pv | awk '{print $1}'

echo '### Verifico utenti presenti'
for delnames in $(oc get users | awk '{print $1}' | grep -E -v 'admin|kubeadmin|developer|NAME'); do
  echo "Elimino utente: $delnames"
  # Trova le identità associate all'utente
  identities=$(oc get user $delnames -o jsonpath='{.identities[*]}')
  
  # Elimina ciascuna identità associata
  for identity in $identities; do
    echo "Elimino identità: $identity"
    oc delete identity $identity
  done
  
  # Elimina l'utente
  oc delete user $delnames
done

echo '### Utenti e identità rimossi con successo!'
oc get users
oc get identities
