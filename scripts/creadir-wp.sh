#!/bin/bash

# Lista dei nomi
nomi=("trabucco" "andronico" "flora" "brusco" "conteduca" "lattanzi" "molinari" "covarelli" "martinelli" "pacciani" "galiani" "giminiani" "ruocco" "bracci")

# Percorso base
percorso_base="/shares/wp"

# Creazione delle directory
for nome in "${nomi[@]}"; do
    mkdir -p "$percorso_base/$nome/data"
done

# Applica permessi alle dir
chmod 777 -R $percorso_base
