#!/bin/bash
# estraggo la ca ingress per utilizzare ansible con "validate_certs: true"
CA_HOME=/usr/share/crc-ca
CA_TRUST=/etc/pki/ca-trust/source/anchors

sudo cp $CA_HOME/ca-cert.crt $CA_HOME/ca-cert.crt_bck
oc get cm kube-root-ca.crt -n openshift-config -o jsonpath='{.data.ca\.crt}' > /tmp/ca-cert.crt
sudo cp /tmp/ca-cert.crt $CA_HOME/ca-cert.crt

# utilizzo la stessa ca per il trust delle rotte interne di crc
# per correggere l'errore "failed to verify certificate: x509: certificate signed by unknown authority"
sudo cp /tmp/ca-cert.crt $CA_TRUST/ca-cert.crt
sudo update-ca-trust