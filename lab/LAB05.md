# Process Template
### Aggiungi postgresql al progetto
* Accedi da shell al tuo progetto come **cluster-admin** ed esegui i seguenti comandi per individuare il template **postgresql-ephemeral**  
```
oc get template -n openshift | grep postgres
oc process postgresql-ephemeral -n openshift --parameters
```
* Processa il template **postgresql-ephemeral** nel tuo progetto
```
oc process postgresql-ephemeral -n openshift -p POSTGRESQL_USER=myuser -p POSTGRESQL_PASSWORD=secret -p POSTGRESQL_DATABASE=mydb | oc apply -f -
``` 
* Accedi alla console di pgadmin https://pgadmin.apps.lab1.aci.it e verifica la connessione al db appena creato con i parametri di autenticazione:
```
# pgadmin login:
url: https://pgadmin.apps.lab1.aci.it
user: student@redhat.com
pass: secret

# connessione postgres
host: postgresql.<cognome>.svc.cluster.local
port: 5432
name: mydb
user: myuser
pass: secret

```
* In caso i pod non si avviano modificate quota e limiti come segue:
``` yaml
# Quotas
spec:
  hard:
    limits.cpu: '4'
    limits.memory: 4Gi
    pods: '8'
    requests.cpu: '2'
    requests.memory: 2500Mi

# Limits
spec:
  limits:
    - type: Container
      max:
        cpu: '1'
        memory: 1Gi
      min:
        cpu: 200m
        memory: 256Mi
      default:
        cpu: 500m
        memory: 512Mi
      defaultRequest:
        cpu: 200m
        memory: 256Mi
    - type: Pod
      max:
        cpu: '1'
        memory: 512Mi
      min:
        cpu: 200m
        memory: 256Mi
```
* Accedere alla console di openshift come **cluster-admin** https://console-openshift-console.apps.lab1.aci.it e crea una **NetworkPolicies** nel tuo progetto che permetta l'accesso solo dal pod di pgadmin nel pod di postgresql con i seguenti parametri:  

```
# Ingress rules
Name: pgadmin-trust
Pod selector: name=postgresql
Namespace selector: team=devsecops
Pod selector: app=pgadmin
Port: TCP/5432
```
