# Load Balancer
### Aggiungi un loadbalancer al tuo progetto wordpress
- Accedi al tuo namespace **(cognome)** con utenza **cluster-admin**  
- Esegui il deploy con nome **balance** usa l'immagine: **docker.io/kerberos5/httpd:scc**  
- Verifica che il pod balance parta correttaemte  
- Crea nel progetto un **serviceaccount** con nome **balance-sa**  
- Dai al **serviceaccount** appena creato l'scc appropriato **anyuid** affinchè il pod parta  
- modifica il file di configurazione **/tmp/corso-aci/httpd-custom.conf** in modo appropriato  
  cambiando con il tuo **(cognome)**  
- Crea ora una **configmap** con nome **httpd-cm** dal file **/tmp/corso-aci/httpd-custom.conf**  
- Inietta la **configmap** come volume nel pod balance al path **/etc/httpd/conf.d/**  
- Rimuovi ora la vecchia **route** che avevi creato nel precedente laboratorio  
- Crea la nuova **route** di tipo **edge** che punti al servizio **balance**  
  la nuova **route** avrà hostname: **(cognome)**.apps.lab1.aci.it  
- Verifica la funzionalità del LB richiamando https://(cognome).apps.lab1.aci.it  

### SOLUZIONE

```
# Accedi come cluster-admin
oc login -u admin -p review

# accedi al tuo progetto
oc project (cognome)

# Crea il loadbalancer
oc new-app --name balance -l app=balance docker.io/kerberos5/httpd:scc

# Crea il serviceaccount
oc create sa balance-sa

# Fornisci al serviceaccount scc anyuid
oc adm policy add-scc-to-user anyuid -z balance-sa

# Setta il deployment in maniera che utilizzi il serviceaccount
oc set serviceaccount deploy balance balance-sa

# Crea una configmap utilizzando il file di configurazione
oc create configmap httpd-cm --from-file httpd-custom.conf

# Setta la configmap come volume nel path predefinito di apache
oc set volumes deploy balance --add --type configmap --configmap-name httpd-cm --mount-path /etc/httpd/conf.d/

# Rimuovi la vecchia route
oc delete route wordpress

# Visulaizza i servizi disponibili
oc get svc

# Verifica che il servizio balance sia integro e funzionante
oc describe svc balance

# Crea la nuova rotta che punti al servizio balance
oc create route edge --service balance --hostname (cognome).apps.lab1.aci.it --port 80
```