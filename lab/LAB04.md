# Template S2i
### Aggiungi apache al progetto
* Accedi da shell al tuo progetto (**cognome**) come **cluster-admin** e importa l'immagine container con il seguente comando:
```
oc import-image apache --from docker.io/kerberos5/httpd:2.4 --confirm -n <cognome>
```
* Verifica che l'immagine sia stata importata correttamente nel tuo progetto
```
oc get is
```
* Accedere alla console di openshift con la propria utenza https://console-openshift-console.apps.lab1.aci.it  
* Spostati nel Tab **Developer** nella sezione **+Add**  
* Cerca e distribusici dal catalogo sviluppatori **Apache HTTP Server** con nome **apache** e tag **latest**
* Per semplificare l'installazione lascia tutti i valori del template di **default** ad eccezione di:
```
name: apache
namespace: <cognome>
tag: latest
```
* Spostati nella sezione **Topology** e verifica il corretto funzionamento del deploy  
* Ora spostati nel tab **Administrator/Workloads** edita il **deployment** e imposta i **limits** e **requests** come segue:  
``` yaml
        - resources:
            limits:
              cpu: 500m
              memory: 512Mi
            requests:
              cpu: 300m
              memory: 256Mi
```
* Dopo il rollout automatico del pod spostati sotto la sezione **pods/metrics** e assicurati che le impostazioni di
  **limits** e **requests** siano state acquisite correttamente.

## SOLUZIONE
### Accedere alla console di Openshift spostati nella sezione Developer

![alt text](img/lab04/image-1.png)

### Nel tab Developer spostati nella sezione +Add

![alt text](img/lab04/image-2.png)

### Cerca e distribusici dal catalogo sviluppatori Apache HTTP Server

![alt text](img/lab04/image-3.png)

### Ora spostati nel tab Administrator e nella sezione Workloads edita il deployment e imposta i limiti

![alt text](img/lab04/image-4.png)

### Verifica che limitis e requests siano state acquisite correttamente  

![alt text](img/lab04/image-5.png)