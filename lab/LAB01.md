# MODULO-01 - DEPLOY APPLICATION

Accedi con utenza cluster-admin
```
oc login -u admin -p ******
```
Crea progetto utilizzando il proprio **cognome**
```
oc new-project <cognome>
```
Fornisci il ruolo di **local** **admin** alla propria utenza **cognome** ristretto solo al proprio progetto **cognome**
```
oc adm policy add-role-to-user admin <cognome> -n <namespace>
```
Crea il secret **review-secret** che contiene le credenziali per mysql
```
oc create secret generic review-secret \
--from-literal USER=wpuser \
--from-literal PASSWORD=redhat123 \
--from-literal DATABASE=wordpress
```

Deploy mysql database con nome **mysql**
```
oc new-app \
--name mysql \
--image registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/rhel8/mysql-80 \
-l app=mysql
```
Monta nel deploy il secret creato in precedenza come variabile specificando il prefisso corretto
```
oc set env deployment/mysql --from secret/review-secret --prefix MYSQL_
```

Deploy l'applicazione **wordpress** specificando in fase di deploy le variabili necessarie
```
oc new-app --name wordpress \
--image registry.gitlab.informatica.aci.it/learning/devops-2024/openshift/tools/redhattraining/wordpress \
-l app=wordpress \
-e WORDPRESS_DB_HOST=mysql \
-e WORDPRESS_DB_NAME=wordpress \
-e WORDPRESS_TITLE=auth-review \
-e WORDPRESS_USER=wpuser \
-e WORDPRESS_PASSWORD=redhat123 \
-e WORDPRESS_EMAIL=student@redhat.com \
-e WORDPRESS_URL=<cognome>.<FQDN>
```

Verifica l'impostazione dell'SCC nel pod di **wordpress**
```
oc get pod/<pod-id> -o yaml |oc adm policy scc-subject-review -f -
```

Crea il serviceAccount **wordpress-sa** e associa la policy corretta per avviare il pod
```
oc create sa wordpress-sa
oc adm policy add-scc-to-user anyuid -z wordpress-sa
oc set sa deployment/wordpress wordpress-sa
```

Imposta il secret **review-secret** come variabile al deployment di **wordpress** con il corretto prefisso
```
oc set env deployment/wordpress --from secret/review-secret --prefix WORDPRESS_DB_
```

Crea la rotta al servizio per esporre **wordpress**
```
oc get svc
oc create route edge wordpress --service wordpress --hostname <cognome>.<FQDN> --port 80
```

# MODULO-02 - PERSISTENZA DEI DATI 

Crea PersistentVolume (**PV**) con le seguenti specifiche

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: <cognome>-mysql-pv
  labels:
    app: <cognome>-mysql
spec:
  capacity:
    storage: 400Mi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs-storage
  nfs:
    path: /shares/wp/<cognome>/data
    server: <IP_NFS_SERVER>
```

Creare un PersistentVolumeClaim (**PVC**) nel proprio namespace **cognome** con le specifiche:

|KEY|VALUE|
|---|-----|
|classe:|nfs-storage|
|nome:|**cognome**-mysql-pvc|
|size:|400MiB|
|label:|app: **cognome**-mysql|

Aggancia il **PVC** al deployment di **mysql**

```
oc set volumes deployment/mysql \
--add \
--type pvc \
--name <cognome>-mysql-pv \
--claim-name <cognome>-mysql-pvc \
--mount-path /var/lib/mysql/data
```

Verfica il bound tra PV/PVC
```
oc get pvc
```

# MODULO-03 - LIMITS AND QUOTAS

Crea quota con nome **my-quota** con le seguenti specifiche

```
apiVersion: v1
kind: ResourceQuota
metadata:
  name: my-quota
  namespace: <cognome>
spec:
  hard:
    limits.cpu: 700m
    limits.memory: 3Gi
    pods: '6'
    requests.cpu: 100m
    requests.memory: 400Mi
```

Ora rimuovi i deployments
```
oc delete pod --all
```

Controlla se i pods sono stati ricreati correttamente
```
oc get pods
```

Crea LimitRanges con nome **my-limits** con le seguenti specifiche
```
apiVersion: v1
kind: LimitRange
metadata:
  name: my-limits
  namespace: <cognome>
spec:
  limits:
    - type: Container
      max:
        cpu: 100m
        memory: 512Mi
      min:
        cpu: 10m
        memory: 40Mi
      default:
        cpu: 100m
        memory: 512Mi
      defaultRequest:
        cpu: 10m
        memory: 40Mi
    - type: Pod
      max:
        cpu: 100m
        memory: 512Mi
      min:
        cpu: 10m
        memory: 40Mi
```	  
Verifica che il servizio sia ora fruibile. Verifica da console il settaggio dei **limits** e l'impiego delle **resources**
Verifica l'effettivo utilizzo delle risorse dei pod
```
oc adm top pod
```

# MODULO-03 - AUTOSCALING
Crea **HPA** per il deploy di wordpress impostando **min-replicas=1** **max-replicas=3** **percentage-cpu=20%**
```
oc autoscale deployment/wordpress --min=1 --max=3 --cpu-percent=20
```

Stressa il pod di **wordpress** con **ApacheBench** e verifica l'attivazione dell'autoscaling
```
./ab.sh 1000 20 https://<cognome>.<FQDN>/
```


