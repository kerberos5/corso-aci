# HELM deploy
### Esegui il deploy di nginx utilizzando HELM Chart
* Accedere alla console di openshift con la propria utenza https://console-openshift-console.apps.lab1.aci.it  
* Spostati nel Tab **Developer** nella sezione **Helm**  
* Aggiungi il repository Helm privato con utilizza le seguenti specifiche:  
```
nome: corso2024
url: http://172.24.225.175/charts
```  
* Esegui il deploy del chart nginx  
* Verifica nel tab Topology la corretta esecuzione e fruibilità del servizio  

## SOLUZIONE
### Accedere alla console di Openshift spostati nella sezione Developer

![alt text](img/lab03/image-1.png)

### Nel tab Developer spostati nella sezione Helm

![alt text](img/lab03/image-2.png)

### Aggiungi il repository Helm privato:  

![alt text](img/lab03/image-3.png)

### Esegui il deploy del chart nginx  

![alt text](img/lab03/image-4.png)

### Verifica nel tab Topology la corretta esecuzione e fruibilità del servizio  

![alt text](img/lab03/image-5.png)

**NOTA!!!** poichè potrebbero esistere più hostname creati allo stesso modo http://my-chart.apps.lab1.aci.it/ editare il proprio ingress e modificare il nome della rotta come ad esempio: 
http://my-chart-cognome.apps.lab1.aci.