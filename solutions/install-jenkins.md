# Installa Jenkins
``` bash
# Crea progetto
oc new-project gitops-deploy

# Per ricavare i template jenkins:
oc get template -n openshift | grep jenkins

# Visualizza i parametri del template:
oc process jenkins-persistent --parameters -n openshift

# Creo il deployment jenkins e gli passo i parametri al template
oc new-app --template jenkins-persistent

# Attribuisci il ruolo edit a serviceaccount jenkins
oc adm policy add-cluster-role-to-user self-provisioner -z jenkins -n gitops-deploy
oc adm policy add-cluster-role-to-user edit -z jenkins -n gitops-deploy
```