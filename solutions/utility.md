# UTILITY

``` bash
# Login to cluster
export KUBECONFIG=/percorso/al/tuo/kubeconfig
oc login

# completion bash oc client
oc completion bash > oc_completion_bash.sh
sudo cp oc_completion_bash.sh /etc/bash_completion.d/oc_completion_bash.sh
sudo chmod 755 /etc/bash_completion.d/oc_completion_bash.sh

# Install e configura helm
sudo dnf install helm -y
helm completion bash > helm-kubernetes.sh
sudo cp helm-kubernetes.sh /etc/bash_completion.d/

# install httpd-tools
sudo yum install httpd-tools -y

# Verifico i pod specifici su un nodo:
oc get pods --field-selector spec.nodeName=<nodeName>

# visualizzo specifici nodi con etichetta specifica
oc get nodes --selector=node-role.kubernetes.io/worker=""

# Visualizzo i pod attestati su nodo infra
oc get pods -o wide --all-namespaces | grep lab1-infr01.lab1.aci.it | awk '{print $1}'

# Applico Label e Taint per i nodi infra
# Aggiungi l'etichetta:
oc label node infra-node-1 node-role.kubernetes.io/infra=""

# Applica il taint:
oc adm taint nodes infra-node-1 node-role.kubernetes.io/infra=:NoSchedule

# Visualizzo gli eventi filtrati per tipo
oc get events --field-selector type=Warning

# Verifica URL console e api
oc whoami --show-console
oc whoami --show-server

# Decrypt secret
oc get secret <nameScret> -n openshift-config -o jsonpath='{.data.*}' | base64 -d

# Exstract/Replace oauth/cluster
oc get oauth cluster -o yaml > oauth.yaml
oc replace -f oauth.yaml

# Visualizzo dove è definito il ruolo self-provisioner
oc get clusterrolebindings -o wide | grep self-provisioner

# Visualizzo lo stato degli operatori
oc get csv -A
oc get sub -A

# Visualizzo label nodes
oc get nodes -L env

# Visualizza Taint per specifici nodi
oc describe nodes --selector=node-role.kubernetes.io/worker="" | grep -i taint

# aggiornare i dati di un secret o configmap
oc set data secret/htpasswd-secret --from-file htpasswd=/tmp/htpasswd -n openshift-config

# Port forward
oc port-forward pod/nginx-app-69b89cfc56-rc5bg 8888:8080
oc port-forward service/nginx-app 8888:8080

# estrai porta container
# Prendendo per assunto che non conosciamo la porta del container avvialo senza specificare porte:
[devops@tmvlocp ~]$ podman run -dit --rm --name nginx quay.io/redhattraining/hello-world-nginx:v1.0

# Preleva il PID del container utilizzando podman inspect
[devops@tmvlocp ~]$ podman inspect nginx | grep -i pid
               "Pid": 26409,
               "ConmonPid": 26407,
          "ConmonPidFile": "/run/user/1000/containers/overlay-containers/8914c7c277a7a20649f9353c02f47b562b209063719167767dfda4a6d7ada12f/userdata/conmon.pid",
          "PidFile": "/run/user/1000/containers/overlay-containers/8914c7c277a7a20649f9353c02f47b562b209063719167767dfda4a6d7ada12f/userdata/pidfile",
               "PidMode": "private",
               "PidsLimit": 2048,

# Ora posso eseguire nsenter per entrare nello spazio dei nomi del processo del contenitore 
# infine eseguire ss -pant per verificare le porte
[devops@tmvlocp ~]$ sudo nsenter -n -t 26409 ss -pant | grep nginx
LISTEN 0      511          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=26412,fd=5),("nginx",pid=26411,fd=5),("nginx",pid=26409,fd=5))
LISTEN 0      511             [::]:8080         [::]:*    users:(("nginx",pid=26412,fd=6),("nginx",pid=26411,fd=6),("nginx",pid=26409,fd=6))

# Pods failing with error loading seccomp filter into kernel: errno 524 in OpenShift 4
# https://access.redhat.com/solutions/7030968
oc get pod -n openshift-controller-manager
NAME                                 READY   STATUS    RESTARTS   AGE
controller-manager-9c9d6fd4f-jnqkh   1/1     Running   0          61m
controller-manager-9c9d6fd4f-pjcm9   1/1     Running   0          5m44s
controller-manager-9c9d6fd4f-t49lk   1/1     Running   0          5m38s

# Accedi ad un nodo del cluster
$ oc debug node/lab1-mast01.lab1.aci.it
sh-4.4# chroot /host bash

# Non persiste al riavvio del nodo
[root@node_name /]# sysctl net.core.bpf_jit_limit=364241152


# Posso ad esempio utilizzare nc specificando il namespace ad esempio workshop-support
oc debug --to-namespace="workshop-support" --  nc -z -v beeper-db.workshop-support.svc.cluster.local 5432

# Oppure posso usare anche curl puntando un diverso namespace esempio default verso il mio pod 
oc debug --to-namespace="default"  -- curl -s http://10.8.0.138:8080
```
