# Installa Elasticsearch e Kibana
``` bash
# Crea il namespace: openshift-operators-redhat per elasticsearch
oc adm new-project openshift-operators-redhat

# Da console installa l'operatore Openshift Elasticsearch Operator scegli (stable-version) e all-namespaces
# Crea il namespace openshift-logging
oc adm new-project openshift-logging

# Da console installa l'operatore Red Hat Openshift Logging 
# scegli in namespace dedicato creato in precedenza penshift-logging
```
``` yaml
# Create PersistentVolume
apiVersion: v1
kind: PersistentVolume
metadata:
  name: elastic-pv
spec:
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs-storage
  nfs:
    path: /shares
    server: tmvlocp.sidi.mpi.it
```
``` yaml
# Create istance ClusterLogging
apiVersion: logging.openshift.io/v1
kind: ClusterLogging
metadata:
  name: instance
  namespace: openshift-logging
spec:
  collection:
    logs:
      fluentd:
        resources:
          limits:
            cpu: 500m
            memory: 736Mi
          requests:
            cpu: 200m
            memory: 736Mi
      type: fluentd
  curation:
    curator:
      schedule: '*/15 * * * *'
    type: curator
  logStore:
    elasticsearch:
      nodeCount: 1
      nodeSelector:
        node-role.kubernetes.io/infra: ''
      redundancyPolicy: ZeroRedundancy
      resources:
        limits:
          cpu: 1
          memory: 4Gi
        requests:
          cpu: 200m
          memory: 4Gi
      storage:
        size: 5G
        storageClassName: nfs-storage
      tolerations:
        - effect: NoSchedule
          key: node-role.kubernetes.io/infra
          operator: Exists
    retentionPolicy:
      application:
        maxAge: 1d
      audit:
        maxAge: 1d
      infra:
        maxAge: 1d
    type: elasticsearch
  managementState: Managed
  visualization:
    kibana:
      proxy:
        resources:
          limits:
            cpu: 500m
            memory: 100Mi
          requests:
            cpu: 100m
            memory: 100Mi
      replicas: 1
      resources:
        limits:
          cpu: 1
          memory: 1Gi
        requests:
          cpu: 500m
          memory: 1Gi
      tolerations:
        - effect: NoSchedule
          key: node-role.kubernetes.io/infra
          operator: Exists
    type: kibana

# Accedi alla console di kibana e crea gli indici app-* e infra-*
```