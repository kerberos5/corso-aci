# PERSISTENZA DATI
``` bash
# Crea directory su NFS
$ mkdir /shares/data
$ sudo chmod -R 777 /shares/data
```
``` yaml
# Crea il PersistentVolume
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pg-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs-storage
  nfs:
    path: /shares/data
    server: <IP_OR_HOSTNAME_NFS_SERVER>
```
``` bash
# Crea PersistentVolumeClaim
oc set volumes deployment/persisting-pg12 \
--add \
--type pvc \
--name pg-pv \
--claim-class nfs-storage \
--claim-mode rwo \
--claim-name pg-pvc \
--claim-size 1G \
--mount-path /var/lib/pgsql/data

# Esegui lo scipt sql per infasare i dati e verifica
oc cp rpi-store-ddl.sql <pod_name>:/tmp
oc rsh <pod_name> bash -c 'psql -U backend -d rpi-store < /tmp/rpi-store-ddl.sql'
oc rsh <pod_name> bash -c 'psql -U backend -d rpi-store -c "SELECT * FROM types;"'
```