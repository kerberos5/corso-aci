# INSTALL NFS SERVER
``` bash
[root@tmvlocp ~]# dnf install nfs-utils -y

[root@tmvlocp ~]# firewall-cmd --add-service={nfs,mountd,rpc-bind} --permanent
[root@tmvlocp ~]# systemctl restart nfs-server.service

[root@tmvlocp ~]# cat /etc/exports
/shares *(rw,sync,no_root_squash,insecure)

[root@tmvlocp ~]# exportfs
/shares         <world>
```
``` yaml
# ESEMPI STORAGE CLASS
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: nfs-storage
provisioner: nfs-client
reclaimPolicy: Retain
volumeBindingMode: Immediate
parameters:
  server: tmvlocp.sidi.mpi.it
  path: /shares

---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: nfs-storage
provisioner: nfs-client
reclaimPolicy: Retain
volumeBindingMode: WaitForFirstConsumer
parameters:
  server: tmvlocp.sidi.mpi.it
  path: /shares
``` 
``` bash
# Crea PV/PVC ed esegui la mount Verifica prima la classe storage che vuoi usare
[devops@tmvlocp ~]$ oc get sc
NAME                                     PROVISIONER                        RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
crc-csi-hostpath-provisioner (default)   kubevirt.io.hostpath-provisioner   Retain          WaitForFirstConsumer   false                  69d
nfs-storage                              nfs-client                         Retain          WaitForFirstConsumer   false                  41d
```
``` yaml
# Puoi creare il PV da console utilizzando l'editor come ad esempio:
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: test-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs-storage
  nfs:
    path: /shares/html
    server: tmvlocp.sidi.mpi.it
```
``` bash
# Ora crea un namespace ad esempio test
[devops@tmvlocp ~]$ oc new-project test

# Deploy una copia di nginx
[devops@tmvlocp ~]$ oc new-app --name test1 quay.io/redhattraining/hello-world-nginx:v1.0

# Puoi creare ora nel namespace test il PVC o da console seguendo la procedura guidata o da riga di comando
[devops@tmvlocp ~]$ oc set volumes deployment/test1 \
--add \
--type pvc \
--name test-pv \
--claim-class nfs-storage \
--claim-mode rwo \
--claim-name test-pvc \
--claim-size 1G \
--mount-path /usr/share/nginx/html

# Puoi verificare con
[devops@tmvlocp ~]$ oc rsh pod/test1-f6fc79498-znrd7 bash -c 'df -h | grep share'
tmvlocp.sidi.mpi.it:/shares/html   99G   74G   23G  77% /usr/share/nginx/html
```