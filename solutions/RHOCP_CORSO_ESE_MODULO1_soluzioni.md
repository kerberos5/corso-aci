# MODULO1

# Task01:
``` bash
oc login -u kubeadmin -p <password> https://api.<FQDN>:6443
oc get clusterversions
oc describe clusterversions
oc adm node-logs <node_name>
debug: oc debug node/<node_name>
ssh -i ~/.crc/machines/crc/id_ecdsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null core@127.0.0.1 -p 2222 "sudo systemctl is-active crio"
ssh -i ~/.crc/machines/crc/id_ecdsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null core@127.0.0.1 -p 2222 "sudo systemctl is-active kubelet"
```

# Task02:
``` bash
# Creo il file che il file che conterrà le credenziali cifrate degli utenti
htpasswd -c -b htpasswd admin review
htpasswd -b htpasswd tester review
htpasswd -b htpasswd leader review
htpasswd -b htpasswd developer review

# Creo il secret che contiene gli utenti
oc create secret generic local-secret –form-file htpasswd=htpasswd -n openshift-config


# Crea il provider custom da console di OCP con nome localusers basato su htpasswd
aggancia il secret local-secret precedentemente creato

# Visualizzo chi possiede il permesso di creare progetti
oc get clusterrolebinding -o wide | grep self-provisioner

# Rimuovo il ruolo self-provisioner in  system:authenticated:oauth in tal modo gli utenti NON admin non possono creare progetti
oc adm policy remove-cluster-role-from-group self-provisioner system:authenticated:oauth

# Crea custom groups
oc adm groups new managers
oc adm groups new editors
oc adm groups new quality

# Aggiungi gli utenti ai gruppi di appartenenza
oc adm groups add-users managers leader
oc adm groups add-users editors developer
oc adm groups add-users quality tester

# Fornisco il ruolo self-provisioner a un gruppo specifico (managers)
oc adm policy add-cluster-role-to-group self-provisioner managers

# Applico le rules ai gruppi
oc adm policy add-cluster-role-to-group edit managers
oc adm policy add-role-to-group edit editors -n auth-review
oc adm policy add-role-to-group view quality -n auth-review

# Restore project creation privileges to all users
oc adm policy add-cluster-role-to-group --rolebinding-name self-provisioners self-provisioner system:authenticated:oauth
```

# Task02: Variante LDAP
``` bash
# Creo secret che conterrà la password di accesso al servizio ldap
oc create secret generic ldap-secret --from-literal=bindPassword=secret -n openshift-config

# Crea una cm con la CA relativa al server LDAP per la comunicazione TLS
oc create configmap ldap-ca --from-file ca.crt=~/ca.crt -n openshift-config
oc get oauth cluster -o yaml > /tmp/oauth-bkp.yaml
```

### Esempio Provider in chiaro:
``` yaml
    - ldap:
        attributes:
          email: []
          id:
            - dn
          name:
            - cn
          preferredUsername:
            - uid
        bindDN: 'cn=admin,dc=redhat,dc=ren'
        bindPassword:
          name: ldap-secret
        insecure: true
        url: 'ldap://ldap-dev.sidi.mpi.it:10389/ou=users,dc=redhat,dc=ren?uid'
      mappingMethod: claim
      name: ldap-dev
      type: LDAP
  templates:
    login:
      name: login-template
  tokenConfig:
    accessTokenMaxAgeSeconds: 0
```

### Esempio Provider in SSL:
``` yaml
    - ldap:
        attributes:
          email: []
          id:
            - dn
          name:
            - cn
          preferredUsername:
            - uid
        bindDN: 'cn=admin,dc=redhat,dc=ren'
        bindPassword:
          name: ldap-secret
        ca:
          name: ldap-ca
        insecure: false
        url: 'ldaps://ldap-dev.sidi.mpi.it:10636/ou=users,dc=redhat,dc=ren?uid'
      mappingMethod: claim
      name: ldap-dev
      type: LDAP
  templates:
    login:
      name: login-template
  tokenConfig:
    accessTokenMaxAgeSeconds: 0
```

# Options:
``` bash
# Effettua il dry-run per verificare la sincronia avvenuta con successo:
oc adm groups sync --sync-config ldap-sync.yaml --confirm
```

# Task03:
``` bash
# Crea un nuovo progetto
oc new-project auth-review

# Crea un secret che contiene le credenziali per mysql
oc create secret generic review-secret \
--from-literal USER=wpuser \
--from-literal PASSWORD=redhat123 \
--from-literal DATABASE=wordpress

# Deploy mysql database
oc new-app --name mysql --image quay.io/redhattraining/mysql-80:1-251 -l app=mysql
oc set env deployment/mysql --from secret/review-secret --prefix MYSQL_

# Deploy wordpress application
oc new-app --name wordpress --image quay.io/redhattraining/wordpress:5.7-php7.4-apache -l app=wordpress \
-e WORDPRESS_DB_HOST=mysql \
-e WORDPRESS_DB_NAME=wordpress \
-e WORDPRESS_TITLE=auth-review \
-e WORDPRESS_USER=wpuser \
-e WORDPRESS_PASSWORD=redhat123 \
-e WORDPRESS_EMAIL=student@redhat.com \
-e WORDPRESS_URL=wordpress-review.<FQDN>

# Verifica l'impostazione dell'SCC nel pod
oc get pod/<pod-id> -o yaml |oc adm policy scc-subject-review -f -

# Crea serviceAccount e associa la policy per avviare correttamente il pod
oc create sa wordpress-sa
oc adm policy add-scc-to-user anyuid -z wordpress-sa
oc set sa deployment/wordpress wordpress-sa

# Set il secret come variabile al deployment
oc set env deployment/wordpress --from secret/review-secret --prefix WORDPRESS_DB_

# Crea la rotta al servizio
oc get svc
oc create route edge wordpress --service wordpress --hostname wordpress-review.<FQDN> --port 80
```

# Task04:
``` bash
# Crea un nuovo progetto
oc new-project pass-route

# Deploy app
oc new-app --name httpd-ssl --image docker.io/kerberos5/httpd:ssl -l app=httpd-ssl

# Crea configmap da file
oc create configmap cm-ssl --from-file=~/corso-aci/httpd-ssl.conf

# Monta la configmap come volume nel container
oc set volumes deployment/httpd-ssl --add --type configmap --configmap-name cm-ssl --mount-path=/etc/httpd/conf.d

# Genera tramite il tool certificato auto-firmato e concatena certificato e CA
cat custom.crt ca.crt > tls.crt
cp custom.key tls.key

# Crea un secret di tipo TLS utilizzando chiave e certificato concatenato
oc create secret tls cert-ssl --cert tls.crt --key tls.key

# Monto il secret come volume nel container
oc set volumes deployment/httpd-ssl --add --type secret --secret-name cert-ssl --mount-path /etc/httpd/cert/

# Creo la rotta di tipo passthrough per cui la terminazione TLS verrà delegata all'applicativo interno al container
oc create route passthrough secure --service httpd-ssl --hostname httpd-ssl.apps.ocp4.redhat.ren --port 8443
```

# Task05:
``` bash
# Creo un nuovo progetto
oc new-project route-review

#Fornisco privilegi edit al gruppo editors
oc adm policy add-role-to-group edit editors -n route-review

# Deploy apache
oc new-app --name web --image docker.io/kerberos5/httpd:2.4 -l app=web

# Creo la rotta di tipo edge con certificato custom il termiatore TLS è delegato al edge router interno di OCP 
oc create route edge web --service web --hostname web.<FQDN> --cert tls.crt --key tls.key
```

# Tasks06:
``` bash
# Crea un nuovo progetto
oc new-project smoke

# Deploy app httpd
oc new-app --name web-httpd --image docker.io/kerberos5/httpd:2.4 -l app=web-httpd

# Creo la rotta
oc create route edge web-httpd --service web-httpd --hostname web-httpd.<FQDN> --port 8080
```

# Tasks07:
``` bash
# installa operatore da console: openshift-integrity-operator
oc get all -n openshift-file-integrity

# Crea ReplicaSet
oc create -f ~/corso-aci/worker-fileintegrity.yaml

# Verifica ReplicaSet
oc get rs -n openshift-file-integrity

# Verifica status nodes
oc get -n openshift-file-integrity fileintegrities/worker-fileintegrity  -o jsonpath="{ .status.phase }" && echo
```

# Tasks08:
``` bash
# Crea due nuovi progetti
oc new-project database
oc new-project checker

# Deploy postgres database
oc new-app --name persisting-pg12 \
-l app=pg12 \
-e POSTGRESQL_USER=backend \
-e POSTGRESQL_PASSWORD=secret_pass \
-e POSTGRESQL_DATABASE=rpi-store \
registry.redhat.io/rhel8/postgresql-12:latest \
-n database

# Deploy pgAdmin webapp
oc new-app --name pgadmin \
-l app=pgadmin \
-e PGADMIN_DEFAULT_EMAIL=student@redhat.com \
-e PGADMIN_DEFAULT_PASSWORD=secret \
docker.io/dpage/pgadmin4:latest \
-n checker

# Verifica SCC costraint
oc get pod/<pod-id> -o yaml |oc adm policy scc-subject-review -f -

# Crea serviceAccount e fornisci privilegi necessari per avviare correttamente l'app
oc create sa pgadmin-sa
oc adm policy add-scc-to-user anyuid -z pgadmin-sa
oc set sa deployment/pgadmin pgadmin-sa

# Crea rotta di tipo edge per pgAdmin
oc create route edge pgadmin --service pgadmin --hostname pgadmin.apps.lab1.aci.it --port 80

# Verifica l'accesso in pgadmin utilizza come host: persisting-pg12.database.svc.cluster.local del db
user: student@redhat.com
pass: secret

# Applica etichetta al progetto checker
oc label namespace/checker team=devsecops

# Applica network policy al progetto database
oc apply -f ~/corso-aci/db-allow-pg12-comm.yaml -n database

# Verifica l'accesso in pgadmin utilizza come host: persisting-pg12.database.svc.cluster.local del db
user: student@redhat.com
pass: secret
```
# Tasks09:
``` bash
# Creo un progetto con ad esempio trabucco
# Avvio un pod da utente trabucco (NON privilegiato)
[devops@tmvlocp ~]$ oc login -u trabucco -p *****
Login successful.
You have one project on this server: "trabucco"
Using project "trabucco".

# Verifico i permessi dell'utente (non può eseguire la discovery dei nodi)
[devops@tmvlocp ~]$ oc get nodes
Error from server (Forbidden): nodes is forbidden: User "trabucco" cannot list resource "nodes" in API group "" at the cluster scope

# Eseguo il deploy dell'applicazione
[devops@tmvlocp ~]$ oc run webphp --image quay.io/redhattraining/webphp:v2
pod/webphp created

# Verifico l'UID assegnato:
[devops@tmvlocp ~]$ oc rsh webphp bash -c id
uid=1000650000(1000650000) gid=0(root) groups=0(root),1000650000

# Lo confronto con quello del namespace trabucco
[devops@tmvlocp ~]$ oc describe project trabucco | grep -i scc
                        openshift.io/sa.scc.mcs=s0:c26,c0
                        openshift.io/sa.scc.supplemental-groups=1000650000/10000
                        openshift.io/sa.scc.uid-range=1000650000/10000

# Eseguendolo come utente normale ha ereditato l'UID definito nel progetto 
# anche se nell'immagine originale l'utente è definito con UID=1001
[devops@tmvlocp ~]$ skopeo inspect docker://quay.io/redhattraining/webphp:v2 --config | jq .config.User
"1001"

# Se ora eseguo il pod con utente privilegiato esempio admin (che è un cluster-admin)
[devops@tmvlocp ~]$ oc login -u admin -p *****
Login successful.
You have access to 67 projects, the list has been suppressed. You can list all projects with 'oc projects'
Using project "trabucco"

# Eseguo nuovamente il deploy dell'app 
[devops@tmvlocp ~]$ oc run webphp --image quay.io/redhattraining/webphp:v2

# Verrà questa volta letto l'utente originale definito nella direttiva dell'immagine USER
[devops@tmvlocp ~]$ oc rsh webphp bash -c id
uid=1001(1001) gid=0(root) groups=0(root)
```