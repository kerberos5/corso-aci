# Task01
``` bash
# Creo un nuovo progetto applicando l'etichetta
oc adm new-project demo --node-selector tier=dev

# applico etichetta ad uno dei nodi del cluster
oc label node/worker01 tier=dev

# Creo un deploy in --dry-run
oc create deployment nginx-dry \
--image quay.io/redhattraining/hello-world-nginx:v1.0 \
--dry-run=client -o yaml > nginx-dry.yaml

# Visualizzo le etichette ed eseguo la describe del pod
oc get node/worker01 --show-labels
oc describe pod -n demo | grep Node-Selectors
```

# Task02:
``` bash
# Crea un nuovo progetto
oc new-project myproject

# Crea quota nel progetto 
oc create quota my-quota --hard=cpu=2,memory=1G,pods=4,services=3
```

# Task03:
``` yaml
# Crea e applica i limits nel progetto
apiVersion: v1
kind: LimitRange
metadata:
  name: my-limits
  namespace: myproject
spec:
  limits:
    - type: Container
      max:
        cpu: 500m
        memory: 512Mi
      min:
        cpu: 400m
        memory: 256Mi
      default:
        cpu: 500m
        memory: 512Mi
      defaultRequest:
        cpu: 400m
        memory: 256Mi
    - type: Pod
      max:
        cpu: 500m
        memory: 512Mi
      min:
        cpu: 400m
        memory: 256Mi
```

# Task04:
``` bash
# Crea il template di progetto
oc adm create-bootstrap-project-template -o yaml > /tmp/project-request.yaml

# Aggiungi le risorse che vuoi che siano create con i nuovi progetti
# come ad esempio ~/corso-aci/yaml/project-request.yaml
# Crea il template in openshift-config
oc create -f /tmp/project-request.yaml -n openshift-config 

# Applica il template editando la risorsa o da console
oc edit projects.config.openshift.io/cluster 
```

# Task05:
``` bash
# Crea il template di progetto
oc new-project gitlab-test

oc new-app --name gitlab --image quay.io/redhattraining/gitlab-ce:8.4.3-ce.0

# Crea serviceaccount dedicato e aggiungi il ruolo scc appropriato al sa
oc create sa gitlab-sa
oc get pod/<pod-id> -o yaml |oc adm policy scc-subject-review -f -
oc adm policy add-scc-to-user anyuid -z gitlab-sa

# Associa quindi il serviceaccount al deploy
oc set serviceaccount deployment/<deploy-name> <sa-name>

# Esegui lo scaling manuale
oc scale deployment/gitlab --replicas=3

# Crea e applica HPA al deployment
oc autoscale deploy gitlab --min=3 --max=5 --cpu-percent=60
oc expose service gitlab --hostname=gitlab.<FQDN> --port 80
```

# Task06:
``` bash
# Crea il template di progetto
oc new-project quota-review
``` 

``` yaml
# Crea e applica le quote al progetto
apiVersion: v1
kind: ResourceQuota
metadata:
  name: my-quota
  namespace: quota-review
spec:
  hard:
    limits.cpu: 700m
    limits.memory: 1200Mi
    pods: '6'
    requests.cpu: 100m
    requests.memory: 400Mi
```
``` bash
# Esegui il deploy
oc new-app --name nginx-test quay.io/redhattraining/hello-world-nginx:v1.0

# Fin quando non vengono applicati i limiti al deploy NON partirà!
oc set resources deployment nginx-test --limits=cpu=200m,memory=512Mi --requests=cpu=100m,memory=256Mi
```