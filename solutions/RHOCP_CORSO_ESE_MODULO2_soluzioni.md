# Task01:
``` bash
# Crea un nuovo progetto
oc new-project app-review

# Deploy nginx utilizzo deployment 
oc new-app --name nginx-app \
-l app=nginx-app \
quay.io/redhattraining/hello-world-nginx:v1.0

# Crea una rotta per l'applicazione
oc create route edge nginx-app --service nginx-app --hostname nginx-app.<FQDN> --port 8080
```

# Task02:
``` bash
# Crea un nuovo progetto
oc new-project dc-review

# Deploy nginx utilizzo deployment-config
oc new-app --name nginx-dc --as-deployment-config --image quay.io/redhattraining/hello-world-nginx:v1.0 -l app=nginx-dc
oc create route edge nginx-dc --service nginx-dc --hostname nginx-dc.<FQDN> --port 8080
```

# Task03:
``` bash
# Crea deployment in dry-run da immagine nginx
oc create deployment nginx-dry --image quay.io/redhattraining/hello-world-nginx:v1.0 --dry-run=client -o yaml > nginx-dry.yaml
oc create route edge nginx-dry --service nginx-dry --hostname nginx-dry.<FQDN> --port 8080
```

# Task04:
``` bash
# Install e configura helm
sudo dnf install helm -y
helm completion bash > helm-kubernetes.sh

# Genera auto-complete per helm
sudo cp helm-kubernetes.sh /etc/bash_completion.d/

# Crea Helm chart
helm create nginx
```
``` yaml
# Edita il Value e inserisci le direttive specificate nell'esercizio
##########################################################
image:
  repository: quay.io/redhattraining/hello-world-nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: "v1.0"

[...]
service:
  type: ClusterIP
  port: 8080

ingress:
  enabled: true
  className: ""
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: my-chart.apps.lab1.aci.it
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local
###############################################
```
``` bash
# Verifica con helm template cosa verrà installato
helm template my-nginx nginx

# Deploy app da chart con helm
helm install my-nginx nginx
helm ls

# Edita in value.yaml
$ vim nginx/values.yaml e scala replica = 2

# Esegui l'update dell'app
helm upgrade my-nginx nginx
helm list -a
oc get pod

# Verifica l'history dell'app
helm history my-nginx

# Esegui rollback dell'app
helm rollback my-nginx 1
oc get pod
```

# Task05:
``` bash
# Crea un nuovo progetto
oc new-project gitlab-test

# Deploy gitlab application
oc new-app --name gitlab --image quay.io/redhattraining/gitlab-ce:8.4.3-ce.0 -l app=gitlab

# crea serviceAccount e attribuisci il corretto ruolo per avviare l'app
oc create sa gitlab-sa
oc get pod/<pod-id> -o yaml |oc adm policy scc-subject-review -f -
oc adm policy add-scc-to-user anyuid -z gitlab-sa
oc set serviceaccount deployment/<deploy-name> <sa-name>
```

# Task06:
``` bash
# Crea un nuovo progetto
oc new-project prb-review

oc create deployment nginx-prb --image quay.io/redhattraining/hello-world-nginx:v1.0 --dry-run=client -o yaml > nginx-prb.yaml
oc get logs -f nginx-prb
oc describe nginx-prb
oc get events
oc edit deployment/nginx-prb
```

# Task07:
``` bash
# Crea un nuovo progetto
oc new-project schedule-pods

# Crea nuovo deployment
oc new-app --name nginx \
-l app=nginx \
quay.io/redhattraining/hello-world-nginx:v1.0

# scala manualmente le repliche
oc scale deployment/nginx --replicas=4

# Applica le etichette ai nodi worker del cluster
oc adm label node/worker1 env=dev
oc adm label node/worker2 env=prod
oc delete all --all -n schedule-pods

# Crea il deployment nginx in dry-run
oc create deployment nginx --image quay.io/redhattraining/hello-world-nginx:v1.0 --dry-run=client -o yaml > nginx.yaml
```
``` yaml
# Modifica il template con nodeSelector: (env:dev)
    spec:
      containers:
      - image: quay.io/redhattraining/hello-world-nginx:v1.0
        name: hello-world-nginx
        resources: {}
      nodeSelector:
        env: dev
```

# Task08:
``` bash
# Crea un nuovo progetto
oc new-project taint-review

# Crea nuovo deployment
oc new-app --name nginx-ta \
-l app=nginx-ta \
quay.io/redhattraining/hello-world-nginx:v1.0

# Applica Taint ad un nodo del cluster
oc adm taint node worker1 planet=earth:NoExecute

# Verifica Taint
oc describe nodes worker1 | grep Taint

oc scale deployment/nginx-ta --replicas=0

# Verifica Taint
oc adm taint node worker1 planet-
```